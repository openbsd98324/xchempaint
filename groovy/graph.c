/*
 *	functions for the graphic output of the pixmap
 */


#include "chemtool.h"


void CreatePix()
{
    XGCValues values;
    XtGCMask mask = GCForeground | GCBackground | GCLineStyle | GCFunction;

    float ztab[]={ 0.6, 0.8, 1};
    int ltab[]={ 0, 2, 2};
    int ftab[]={ 3, 5, 6};

    size_factor=ztab[zoom_factor];
    head.pix_width=head.width*size_factor;
    head.pix_height=head.height*size_factor;

    picture = XCreatePixmap(XtDisplay(pix),
	RootWindowOfScreen(XtScreen(pix)),
	head.pix_width, head.pix_height, 1); 

    values.foreground=0;
    values.background=1;
    values.line_style=LineSolid;
    values.function=GXcopy;
    cleargc = XCreateGC(XtDisplay(pix), picture, mask, &values);

    values.foreground=1;
    values.background=0;
    values.line_style=LineSolid;
    values.function=GXcopy;
    drawgc =  XCreateGC(XtDisplay(pix), picture, mask, &values);

    values.foreground=1;
    values.background=0;
    values.line_style=LineSolid;
    values.function=GXinvert;
    invgc = XCreateGC(XtDisplay(pix), picture, mask, &values);

    if(!Load_Font(ftab[zoom_factor])){
    if(font = XLoadQueryFont(XtDisplay(pix), "fixed"))
	XSetFont(XtDisplay(pix), drawgc, font->fid);
    else {
	fprintf(stderr, "chemtool: can't load any font\n"); exit(1);
	}
    }
    Set_Line(ltab[zoom_factor]);
    XtVaSetValues(pix, XtNwidth, head.pix_width, 
    XtNheight, head.pix_height, NULL);
}

void FreePix(void)
{
    XFreeGC(XtDisplay(pix), invgc);
    XFreeGC(XtDisplay(pix), drawgc);
    XFreeGC(XtDisplay(pix), cleargc);
    XFreePixmap(XtDisplay(pix), picture);
}

void CopyPlane(void)
{
    XCopyPlane(XtDisplay(pix), picture, XtWindow(pix), 
    DefaultGCOfScreen(XtScreen(pix)), 
    0, 0, head.pix_width, head.pix_height, 0, 0, 1);
}

void Display_Mol(void)
{
    struct data *hpc;
    struct dc *hpc_c;
    int xb,yb,d;
    char *c;
    struct xy_co *coord;

    XFillRectangle(XtDisplay(pix), picture, cleargc, 0, 0, 
		      head.pix_width, head.pix_height);

     hpc=da_root.next;
     for(d=0; d<hp.n; d++){
	  if(!hpc->bound) Drawline(hpc->x, hpc->y, hpc->tx, hpc->ty);
	  if(hpc->bound==4){
	      coord=center_double_bound(hpc->x, hpc->y, hpc->tx, hpc->ty, 4);
	      Drawline(coord->x, coord->y, coord->tx, coord->ty);
	      coord++;
	      Drawline(coord->x, coord->y, coord->tx, coord->ty);
	      }
	  if(hpc->bound==1 || hpc->bound==3){
	      coord=multi_bounds(hpc->x, hpc->y, hpc->tx, hpc->ty, 10);
	      Drawline(coord->x, coord->y, coord->tx, coord->ty);
	      Drawline(hpc->x, hpc->y, hpc->tx, hpc->ty);
	      }
	  if(hpc->bound==2 || hpc->bound==3){
	      coord=multi_bounds(hpc->tx, hpc->ty, hpc->x, hpc->y, 10);
	      Drawline(coord->x, coord->y, coord->tx, coord->ty);
	      Drawline(hpc->x, hpc->y, hpc->tx, hpc->ty);
	      }
	   hpc=hpc->next; 
    }

    hpc_c=dac_root.next;
    for(d=0; d<hp.nc; d++){
	 c=hpc_c->c;
	 Drawstring(hpc_c->x, hpc_c->y, c, hpc_c->direct);
	 hpc_c=hpc_c->next; 
	 }

    if(mark.flag){
	 XDrawRectangle(XtDisplay(pix), picture, drawgc, 
	 mark.x*size_factor, mark.y*size_factor, 
	 mark.w*size_factor, mark.h*size_factor);
    }
    CopyPlane();
}

void Drawline(int x, int y, int tx, int ty)
{
    XDrawLine(XtDisplay(pix), picture, drawgc,
    x*size_factor, y*size_factor, tx*size_factor, ty*size_factor);
}


void Drawstring(int x, int y, char *c, int direct)
{
    int ha, htw, tw, a;
    int nch, textl, chl;
    char *l, *hc, hl[2];
    int d, hx, v=0, n;
    int variance[MAXCL];
    char text[MAXCL];

    x=x*size_factor;
    hx=x;
    y=y*size_factor;
    ha=font->ascent/2;
    a=font->ascent;

    n = strlen(c);

    hc=c;
    for(d=0;d<n;d++){
      if(variance[d]=Extra_char(*hc)) {hc++;--n;}
      text[d]=*hc++;
    }
    
    nch=n;
    if(nch>1){
	 if(variance[nch-1]) --nch;
	 textl = XTextWidth(font, c, nch);
	 chl = (textl/nch)/2;
	 hx = x + (textl/2-chl )*direct;
    }
    nch=n;

    x=hx;
    for(d=0;d<n;d++){
	  hl[0]=text[d]; hl[1]=0; l=&hl[0];
	  tw = XTextWidth(font, l, strlen(l)); htw = tw/2;
	  XFillArc(XtDisplay(pix), picture, cleargc, 
			  x-htw - 7*size_factor, 
			  y-a+ha - 5*size_factor + variance[d],
			  htw*2 + 12*size_factor,
			  a + 12*size_factor,
			  0, 360*64);
	  x=x+tw;
    }

    n=nch;
    x=hx;
    for(d=0;d<n;d++){
	  hl[0]=text[d]; hl[1]=0; l=&hl[0];
	  tw = XTextWidth(font, l, strlen(l)); htw = tw/2;
	  XDrawString(XtDisplay(pix), picture, drawgc, 
	  x-htw, y+ha+variance[d], l, strlen(l));
	  x=x+tw;
    }
}

int Extra_char(char c)
{
    int ha=font->ascent/2;
    if(c == '_') return(ha);
    if(c == '^') return(-ha);
    return(0);
}

int Load_Font(int fnt)
{
    String fntype[] = {"5x7", "6x10", "fixed", "9x15", "8x16", 
    "10x20", "12x24"};

    if(fnt>6) return(0);
    if(font = XLoadQueryFont(XtDisplay(pix), fntype[fnt])){
	      XSetFont(XtDisplay(pix), drawgc, font->fid);
	      XSetFont(XtDisplay(pix), cleargc, font->fid);
	      return(1);}
    else return(0);
}

void Set_Line(int lsty)
{
    XSetLineAttributes(XtDisplay(pix), gc, lsty, 0, CapButt, JoinMiter);
    XSetLineAttributes(XtDisplay(pix), drawgc, lsty, 0, CapButt, JoinMiter);
    XSetLineAttributes(XtDisplay(pix), cleargc, lsty, 0, CapButt, JoinMiter);
    XSetLineAttributes(XtDisplay(pix), invgc, lsty, 0, CapButt, JoinMiter);
}
