#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <X11/Shell.h>

#include <X11/Xaw/Cardinals.h>
#include <X11/Xaw/Paned.h>
#include <X11/Xaw/Box.h>
#include <X11/Xaw/Command.h>
#include <X11/Xaw/AsciiText.h>
#include <X11/Xaw/Toggle.h>
#include <X11/Xaw/Viewport.h>
#include <X11/Xaw/Label.h>
#include <X11/xpm.h>

#define VERSION "1.2"
#define PIXMAPWIDTH 900
#define PIXMAPHEIGHT 500

#define MAXCL 80
#define Hex 1    /* define angletypes and textdirect. , DONT CHANGE THEM !*/
#define Pent 2
#define Pent_switch 3
#define Octa 4
#define No_angle 0
#define Left_Text 0 
#define Middle_Text -1
#define Right_Text -2

struct data {
            int x,y;
            int tx,ty;
	    int bound;
	    struct data *next;
	    struct data *prev; 
	    } da_root, *new;

struct dc {
	    int x,y;
	    char c[MAXCL];
	    int direct;
	    struct dc *next;
	    struct dc *prev; 
  	  } dac_root, *new_c;

struct data_head {
	    Dimension pix_width, pix_height;
	    Dimension width, height;
	    } head;
  
struct new {
	   int x,y;
	   int tx,ty;
	   int n, nc;
	   } hp; 

struct xy_co {
	    int x,y;
	    int tx,ty;
	    } xycoord[30];

struct marked {
	   int x,y;
	   int w, h;
	   int flag;
	   } mark; 


extern Widget  toplevel, pix, pixbox;
extern Widget atom_dialog, dialog, dialogbox, okdial, canceldial;
extern Widget load, hexa, canvas;
extern Widget exportdialog;
extern Pixmap picture;
extern GC gc, cleargc, invgc, drawgc;
extern XFontStruct *font;

extern Pixel fgr, bgr;

XtTranslations ctrans1, catom, crec, cdel, cmark, cmbound, cpmove;

float size_factor; 
int zoom_factor;

int draw_angle;
int text_direct;
int font_type;
int line_type;
int modify;

/*	chemproc.c	*/

extern struct data *select_vector(int mx, int my, float s_f);
extern void add_struct(int x,int y,int tx, int ty, int bound);
extern int myround(int i, float step);
extern void del_char(struct dc *hpc);
extern void del_struct(struct data *hpc);
extern void add_char(int x,int y, char *c, int direct);
extern struct dc *select_char(int mx, int my, float s_f);
extern struct xy_co *calc_angle(int x, int y, int ang_type, int angle);
extern struct xy_co *position(int x, int y, int ax, int ay, int ang_type);
extern void setup_data(void);
extern void clear_data(void);
extern int partial_delete(int x, int y, int tx, int ty);
extern void center_mol(void);
extern int move_pos(int x, int y, int tx, int ty);
extern int check_pos_rec(int x, int y, int rx, int ry, int rtx, int rty);
extern int partial_move(int x, int y, int tx, int ty, int dx, int dy);

extern int calc_vector(int dx, int dy);
extern struct xy_co *multi_bounds(int mx, int my, int mtx, int mty, int r);
extern struct xy_co *center_double_bound(int mx,int my,int mtx, int mty, int r);
struct xy_co *bound_cut(int x, int y, int tx, int ty, int r);

/*	graph.c	*/

extern void CreatePix();
extern void FreePix();
extern void CopyPlane();
extern void Display_Mol();
extern void Drawline();
extern void Drawstring();
extern int Extra_char(char c);
extern int Load_Font();
extern void Set_Line(int lsty);

/*	draw.c	*/

extern void Add_double();
extern void Add_vector();
extern void Del_vector();
extern void Set_vector();
extern void Put_vector();
extern void Add_atom();
extern void Del_atom();
extern void Set_start_rec();
extern void Put_rec();
extern void Del_rec();
extern void Mark_rec();
extern void Center();
extern void Put_pmove();
extern void Set_pmove();

/*	dialog.c	*/

extern void  Puttext();
extern Widget MakeStringBox();
extern String GetString();
extern void Load();
extern void Save();
extern void Export();
extern void Close_Export();
extern void XBM_Export();
extern void XPM_Export();
extern void Xfig_Export();
extern void LaTeX_Export();
extern void Quit();
extern void Really_Quit();
extern void Zoom();
extern void Change_Text();
extern void Change_Angle();
extern void Edit_Mol();
extern void UnMark();
extern Widget toggle_icon();

/*	inout.c	*/

extern int save_mol(char *filename);
extern int load_mol(char *filename);
extern int export_bitmap(char *filename);
extern int export_xfig(char *filename);
extern int export_latex_pic(char *filename);
