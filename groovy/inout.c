/*
 *	functions for in and output
 */


#include "chemtool.h"
#include "inout.h"

int save_mol(char *filename)
{
    int d;
    struct data *hp_b;
    struct dc *hp_a;
    FILE *fp;

    if((fp = fopen(filename, "w")) == NULL) return(1); 

    fprintf(fp, "Chemtool Version "VERSION"\n");

    fprintf(fp, "geometry %i %i\n", head.width, head.height);

    hp_b=da_root.next;
    fprintf(fp, "bounds %i\n",hp.n);
    for(d=0; d<hp.n; d++){
	 fprintf(fp, "%i	%i	%i	%i	%i\n",
		    hp_b->x, hp_b->y, hp_b->tx, hp_b->ty, hp_b->bound); 
	 hp_b=hp_b->next; 
	}

    hp_a=dac_root.next;
    fprintf(fp, "atoms %i\n",hp.nc);
    for(d=0; d<hp.nc; d++){
	 fprintf(fp, "%i	%i	%s	%i\n",
		    hp_a->x, hp_a->y, hp_a->c, hp_a->direct );
	 hp_a=hp_a->next; 
	}

    fclose(fp);
    return(0);
}

int load_mol(char *filename)
{
    int d,n;
    int x,y,tx,ty,b;
    char str[255];
    FILE *fp;
    char version[10];

    if((fp = fopen(filename, "r")) == NULL) return(1); 

    fscanf(fp, "%s %s %s", str, str, version);
    if(atof(version) > atof(VERSION)) return(2);

    clear_data();

    fscanf(fp, "%s %i %i", str, &x, &y);
    if(!strcmp(str, "geometry")) {
	   head.width=(Dimension) x;
	   head.height=(Dimension) y;
    }

    fscanf(fp, "%s %i", str, &n);
    if(!strcmp(str,"bounds")){
	   for(d=0;d<n;d++){
		fscanf(fp, "%i %i %i %i %i",&x,&y,&tx,&ty,&b);
		add_struct(x, y, tx, ty, b);
		}
    }

    fscanf(fp, "%s %i", str, &n);
    if(!strcmp(str,"atoms")){
	   for(d=0;d<n;d++){
		fscanf(fp, "%i %i %s %i",&x,&y,str, &b);
		add_char(x, y, str, b);
		}
    }

    fclose(fp);
    return(0);
}

int export_bitmap(char *filename)
{
    int size, x, y, w, h;
    Pixmap buffer;
    char xfile[512];

    if(mark.flag){
	x=mark.x*size_factor+3;
	y=mark.y*size_factor+3;
	w=mark.w*size_factor-6;
	h=mark.h*size_factor-6;
    }else{
	x=0; y=0;
	w=head.pix_width; h=head.pix_height;
    }

    buffer = XCreatePixmap(XtDisplay(pix),
		 RootWindowOfScreen(XtScreen(pix)),
		 w, h, 1);

    XCopyPlane(XtDisplay(pix), picture, buffer, drawgc,
	x, y, w, h, 0, 0, 1);


    if(strlen(filename)) {
	    sprintf(xfile, "%s.xbm", filename);
	    XWriteBitmapFile(XtDisplay(pix), xfile, buffer, w, h, -1, -1);
    }
    XFreePixmap(XtDisplay(pix), buffer);
}

void xfig_line(FILE *fp, int x, int y, int tx, int ty)
{
    fprintf(fp, "%i %i %i %i %i %i %i %i %i %f %i %i %i %i %i %i\n",
	figline.object, figline.sub_type, figline.line_style,
	figline.thickness, figline.pen_color, figline.fill_color,
	figline.depth, figline.pen_style, figline.area_fill,
	figline.style_val, figline.join_style, figline.cap_style,
	figline.radius, figline.forward_arrow, figline.backward_arrow, 
	figline.npoints);
    fprintf(fp, "%i %i %i %i\n", x, y, tx, ty);
}

int exfig(char *filename, int latex_flag)
{
    struct data *hp_b;
    struct dc *hp_a;
    struct xy_co *coord;

    FILE *fp;
    int d, x, y, tx, ty;
    int ha, chl;
    int fontsize=12, fzoom=10;

    switch(zoom_factor){
	case 0:
	    fontsize=6;
	    fzoom=5;
	    break;
	case 1:
	    fontsize=9;
	    fzoom=7;
	    break;
	case 2:
	    fontsize=12;
	    fzoom=10;
	    break;
    }

    if((fp = fopen(filename, "w")) == NULL) return(1);
    fprintf(fp, "#FIG 3.2\n");
    fprintf(fp, "Landscape\n");
    fprintf(fp, "Center\n");
    fprintf(fp, "Inches\n");
    fprintf(fp, "Letter\n");
    fprintf(fp, "100.00\n");
    fprintf(fp, "Single\n");
    fprintf(fp, "0\n");
    fprintf(fp, "1200 2\n");

    figline.object=2;
    figline.sub_type=1;
    figline.line_style=0;
    figline.thickness=1;
    figline.pen_color=0;
    figline.fill_color=7;
    figline.depth=0;
    figline.pen_style=0;
    figline.area_fill=-1;
    figline.style_val=0.000;
    figline.join_style=0;
    figline.cap_style=0;
    figline.radius=-1;
    figline.forward_arrow=0;
    figline.backward_arrow=0;
    figline.npoints=2;

    hp_b=da_root.next;
    for(d=0; d<hp.n; d++){
	coord=bound_cut(hp_b->x, hp_b->y, hp_b->tx, hp_b->ty, 12);
	x=coord->x*fzoom;
	y=coord->y*fzoom;
	tx=coord->tx*fzoom;
	ty=coord->ty*fzoom;
	if(!hp_b->bound) xfig_line(fp, x, y, tx, ty);
	if(hp_b->bound==4){
	    coord=center_double_bound(x, y, tx, ty, 4*fzoom);
	    xfig_line(fp, coord->x, coord->y, coord->tx, coord->ty);
	    coord++;
	    xfig_line(fp, coord->x, coord->y, coord->tx, coord->ty);
	    }
	if(hp_b->bound==1 || hp_b->bound==3){
	    coord=multi_bounds(x, y, tx, ty, 10*fzoom);
	    xfig_line(fp, x, y, tx, ty);
	    xfig_line(fp, coord->x, coord->y, coord->tx, coord->ty);
	    }
	if(hp_b->bound==2 || hp_b->bound==3){
	    coord=multi_bounds(tx, ty, x, y, 10*fzoom);
	    xfig_line(fp, x, y, tx, ty);
	    xfig_line(fp, coord->x, coord->y, coord->tx, coord->ty);
	    }
	hp_b=hp_b->next; 
    }

    figtext.object=4;
    figtext.sub_type=0;
    figtext.color=0;
    figtext.depth=0;
    figtext.pen_style=0;
    figtext.font=12;
    figtext.font_size=fontsize;
    figtext.angle=0.000;
    figtext.font_flags=6;
    figtext.height=135;
    figtext.length=405;

    ha=9*fzoom;

    hp_a=dac_root.next;
    for(d=0; d<hp.nc; d++){
	figtext.sub_type=abs(hp_a->direct);
	switch(hp_a->direct){
	    case Middle_Text:
		chl=0;
		break;
	    case Left_Text:
		chl = 9*fzoom;
		break;
	    case Right_Text:
		chl = -9*fzoom;
		break;
	}
	if(latex_flag){
	    fprintf(fp, "%i %i %i %i %i %i %f %f %i %f %f %i %i $%s$\\001\n",
		figtext.object, figtext.sub_type, figtext.color,
		figtext.depth, figtext.pen_style, figtext.font,
		figtext.font_size, figtext.angle, figtext.font_flags,
		figtext.height, figtext.length,
		hp_a->x*fzoom-chl, hp_a->y*fzoom+ha, hp_a->c);
	}else{
	    fprintf(fp, "%i %i %i %i %i %i %f %f %i %f %f %i %i %s\\001\n",
		figtext.object, figtext.sub_type, figtext.color,
		figtext.depth, figtext.pen_style, figtext.font,
		figtext.font_size, figtext.angle, figtext.font_flags,
		figtext.height, figtext.length,
		hp_a->x*fzoom-chl, hp_a->y*fzoom+ha, hp_a->c);
	}
	hp_a=hp_a->next; 
    }

    fclose(fp);
    return(0);
}

int export_xfig(char *filename)
{
    char xfile[512];

    if(strlen(filename)) {
	sprintf(xfile, "%s.fig", filename);
	exfig(xfile, 0);
    }
}

int export_latex_pic(char *filename)
{
    char com[255];
    char xfile[512];

    if(strlen(filename)) {
	sprintf(com, "fig2dev -Lpictex %s.f2l %s.tex", filename, filename);
	sprintf(xfile, "%s.f2l", filename);
	exfig(xfile, 1);
	system(com);
    }
}
