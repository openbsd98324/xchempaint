/*
 *	functions for calculation and memory-management
 */


#include "chemtool.h"


void del_struct(struct data *hpc)
{
    if(hp.n && hpc) {
	(hpc->prev)->next=hpc->next;
	(hpc->next)->prev=hpc->prev;
	free(hpc);
	hp.n--;
    }
}

struct dc *select_char(int mx, int my, float s_f)
{
    int x,y,d;
    struct dc *hpc;
    struct xy_co *coord;

    x=myround(mx, s_f);
    y=myround(my, s_f);
    coord=position(x, y, x, y, No_angle);
    x=coord->x;
    y=coord->y;

    hpc=dac_root.next;
    for(d=0;d<hp.nc;d++){
	if(x==hpc->x && y==hpc->y) return(hpc);
	hpc=hpc->next; 
    }
    return(NULL);
}

void add_char(int x,int y, char *c, int direct)
{
    struct dc *last;

    strcpy(new_c->c, c);
    new_c->x=x;
    new_c->y=y;
    new_c->direct=direct;
    new_c->next=(struct dc *) malloc(sizeof(struct dc));
    last=new_c;
    new_c=new_c->next; hp.nc++;
    new_c->prev=last; 
    new_c->next=NULL;
}

void del_char(struct dc *hpc)
{
    if(hp.nc && hpc) {
	(hpc->prev)->next=hpc->next;
	(hpc->next)->prev=hpc->prev;
	free(hpc);
	hp.nc--;
    }
}

int myround(int i, float step)
{
    int h;

    h=i/step;
    if(((float) i/step - h) > 0.5 ) h++;
    return(h);
}

void add_struct(int x,int y,int tx, int ty, int bound)
{
    struct data *last;

    new->x=x;
    new->y=y;
    new->tx=tx;
    new->ty=ty;
    new->bound=bound;
    new->next=(struct data *) malloc(sizeof(struct data));
    last=new;
    new=new->next; hp.n++;
    new->prev=last; 
    new->next=NULL;
}

struct data *select_vector(int mx, int my, float s_f)
{
    int v1,v2,v,d,x,y,tx,ty;
    struct data *hpc;

    hpc=da_root.next;
    for(d=0;d<hp.n;d++){
	x=hpc->x*s_f;
	tx=hpc->tx*s_f;
	y=hpc->y*s_f;
	ty=hpc->ty*s_f;
	v=calc_vector(abs(tx-x), abs(ty-y));
	v1=calc_vector(abs(mx-x), abs(my-y));
	v2=calc_vector(abs(tx-mx), abs(ty-my));
	/* if(v1 + v2 <= v) return(hpc);*/
	if((v1*v1 + v2*v2)*1.5 < v*v) return(hpc);
	hpc=hpc->next; 
    }
    return(NULL);
}

struct xy_co *multi_bounds(int mx, int my, int mtx, int mty, int r)
{
    int x=0,y=0,tx=0,ty=0,dx,dy;
    struct xy_co *coord=&xycoord[0];
    float alpha;
    int v;

    dx=mtx - mx;
    dy=mty - my;
    v = calc_vector(dx, dy);
    alpha=(float) asin((float) dx/v);
    if( dx <0 && dy <0 || !dy){
	  x=r*cos(alpha-M_PI_4);
	  y=r*sin(alpha-M_PI_4);
	  tx=r*cos(alpha+M_PI_4);
	  ty=r*sin(alpha+M_PI_4);
      }
    if( dx >0 && dy >0){
	  x=-r*cos(alpha+M_PI_4);
	  y=r*sin(alpha+M_PI_4);
	  tx=-r*cos(alpha-M_PI_4);
	  ty=r*sin(alpha-M_PI_4);
      }
    if( dx <0 && dy >0){
	  x=-r*cos(M_PI+alpha-M_PI_4);
	  y=r*sin(M_PI+alpha-M_PI_4);
	  tx=r*cos(alpha+M_PI_4);
	  ty=-r*sin(alpha+M_PI_4);
      }
    if( dx >0 && dy <0){
	  x=-r*cos(alpha+M_PI_4);
	  y=-r*sin(alpha+M_PI_4);
	  tx=-r*cos(alpha-M_PI_4);
	  ty=-r*sin(alpha-M_PI_4);
      }
    if(!dx && dy<0){
	  x=-r*cos(M_PI_4);
	  y=-r*sin(M_PI_4);
	  tx=-r*cos(M_PI_4);
	  ty=r*sin(M_PI_4);
      }
    if(!dx && dy>0){
	  x=r*cos(M_PI_4);
	  y=r*sin(M_PI_4);
	  tx=r*cos(M_PI_4);
	  ty=-r*sin(M_PI_4);
      }
    coord->x=mx + x; 
    coord->y=my + y;
    coord->tx=mtx + tx;
    coord->ty=mty + ty;
    return(coord);
}

struct xy_co *center_double_bound(int mx, int my, int mtx, int mty, int r)
{
    int x=0,y=0,tx=0,ty=0;
    int sx=0,sy=0,stx=0,sty=0;
    struct xy_co *coord=&xycoord[0];
    struct xy_co *coord2=&xycoord[1];
    float alpha;
    int v, dx, dy;;

    dx=mtx - mx;
    dy=mty - my;
    v = calc_vector(dx, dy);
    alpha=(float) asin((float) dx/v);
    if(!dy){
	  y=-r*sin(alpha-M_PI);
	  ty=-r*sin(alpha-M_PI);
	  sy=r*sin(alpha-M_PI);
	  sty=r*sin(alpha-M_PI);
      }
    if(!dx){
	  x=-r*cos(M_PI);
	  tx=-r*cos(M_PI);
	  sx=r*cos(M_PI);
	  stx=r*cos(M_PI);
      }
    coord->x=mx + x; 
    coord->y=my + y;
    coord->tx=mtx + tx;
    coord->ty=mty + ty;
    coord2->x=mx + sx; 
    coord2->y=my + sy;
    coord2->tx=mtx + stx;
    coord2->ty=mty + sty;
    return(coord);
}

int calc_vector(int dx, int dy)
{
    return((int) sqrt(dx*dx + dy*dy));
}

struct xy_co *calc_angle(int x, int y, int ang_type, int angle)
{
    int dx,dy;
    struct xy_co *coord=&xycoord[0];

    int hex[]={0,	-64, 32, -55, 55, -32,
		64,	0, 55, 32, 32,	55,
		0, 64, -32, 55, -55, 32,
		-64, 0, -55, -32, -32, -55};
    int pent[]={0, -64, 37, -51, 
		  60, -19, 60, 19, 
		  37, 51, 0, 63,
		 -37, 51, -60, 19,
		 -60, -19, -37, -51};
    int pent_sw[]={19, -60, 51, -37,
		63, 0, 51, 37,
		19, 60, -19, 60,
		-51, 37, -63, 0,
		-51, -37, -19, -60};
    int octa_angle[]={  0,      64,
			45,     45,
			63,     0,
			45,     -45,
			0,      -63,
			-45,    -45,
			-63,    0,
			-45,    45};

    switch(ang_type){ 
	case Hex:
	  coord->x=x+hex[angle*2];
	  coord->y=y+hex[angle*2+1];
	  break;
	case Pent:
	  coord->x=x+pent[angle*2];
	  coord->y=y+pent[angle*2+1];
	  break;
	case Pent_switch:
	  coord->x=x+pent_sw[angle*2];
	  coord->y=y+pent_sw[angle*2+1];
	  break;
	case Octa:
	  coord->x=x+octa_angle[angle*2];
	  coord->y=y+octa_angle[angle*2+1];
	  break;
    }
    return(coord);
}

struct xy_co *position(int x, int y, int ax, int ay, int ang_type)
{
    int angle_count[]={0, 12, 10, 10, 8};
    int d, dmax, dx, dy, v, bx, by ,bv;
    int hx, hy;
    struct dc *hp_a;
    struct data *hp_b;
    struct xy_co *coord=&xycoord[0];
    int *angco;
      
    bx=x;
    by=y;
    bv=16;

    hp_b=da_root.next;
    for(d=0; d<hp.n; d++){
	dx=abs(x - hp_b->x);
	dy=abs(y - hp_b->y);
	v=calc_vector(dx,dy);
	if(v<bv) {bv=v; bx=hp_b->x; by=hp_b->y;}
	dx=abs(x - hp_b->tx);
	dy=abs(y - hp_b->ty);
	v=calc_vector(dx,dy);
	if(v<bv) {bv=v; bx=hp_b->tx; by=hp_b->ty;}
	hp_b=hp_b->next; 
    }

    hp_a=dac_root.next;
    for(d=0; d<hp.nc; d++){
	dx=abs(x - hp_a->x);
	dy=abs(y - hp_a->y);
	v=calc_vector(dx,dy);
	if(v<bv) {bv=v; bx=hp_a->x; by=hp_a->y;}
	hp_a=hp_a->next; 
    }
  
    dmax=angle_count[ang_type];
    for(d=0;d<dmax;d++){
	coord=calc_angle(ax, ay, ang_type, d);
	hx=coord->x; hy=coord->y;
	dx=abs(x - hx);
	dy=abs(y - hy);
	v=calc_vector(dx,dy);
	if(v<bv) {bv=v; bx=hx; by=hy;}
    }
    coord->x=bx;
    coord->y=by;
    return(coord);
}

void setup_data(void)
{
    new=&da_root;
    new->prev=NULL;
    new->next=(struct data *) malloc(sizeof(struct data));
    new=new->next;
    new->prev=&da_root;

    new_c=&dac_root;
    new_c->prev=NULL;
    new_c->next=(struct dc *) malloc(sizeof(struct dc));
    new_c=new_c->next;
    new_c->prev=&dac_root;

    hp.n=0;
    hp.nc=0;
}

void clear_data(void)
{
    struct data *hp_b;
    struct dc *hp_a;
    int hn, hnc, d;

    hn=hp.n;
    hnc=hp.nc;

    hp_b=da_root.next;
    for(d=0; d<hn; d++){
	   del_struct(hp_b);
	   hp_b=hp_b->next; 
    }

    hp_a=dac_root.next;
    for(d=0; d<hnc; d++){
	   del_char(hp_a);
	   hp_a=hp_a->next; 
    }
}

int check_pos_rec(int x, int y, int rx, int ry, int rtx, int rty)
{
    if(x > rx && y > ry && x < rtx && y < rty) return(1);
    return(0);
}

int partial_delete(int x, int y, int tx, int ty)
{
    int d, r=False;
    int hn;
    struct data *hp_bound;
    struct dc *hp_atom;

    hp_atom=dac_root.next;
    hn=hp.nc;
    for(d=0; d<hn; d++){
       if(check_pos_rec(hp_atom->x, hp_atom->y, x, y, tx, ty)) 
	   {
	   del_char(hp_atom); r=True;
	   }
       hp_atom=hp_atom->next;
    }

    hp_bound=da_root.next;
    hn=hp.n;
    for(d=0; d<hn; d++){
       if(check_pos_rec(hp_bound->x, hp_bound->y, x, y, tx, ty) ||
	   check_pos_rec(hp_bound->tx, hp_bound->ty, x, y, tx, ty))
	   {
	   del_struct(hp_bound); r=True;
	   }
       hp_bound=hp_bound->next;
    }

    return(r);
}

void center_mol(void)
{
    struct data *hp_bound;
    struct dc *hp_atom;
    int xmax=0, ymax=0;
    int xmin=head.width, ymin=head.height;
    int d;

    if(hp.n || hp.nc){ 

	hp_atom=dac_root.next;
	for(d=0; d<hp.nc; d++){
	       if(hp_atom->x > xmax) xmax=hp_atom->x;
	       if(hp_atom->y > ymax) ymax=hp_atom->y;
	       if(hp_atom->x < xmin) xmin=hp_atom->x;
	       if(hp_atom->y < ymin) ymin=hp_atom->y;
	       hp_atom=hp_atom->next;
	}

	hp_bound=da_root.next;
	for(d=0; d<hp.n; d++){
	       if(hp_bound->x > xmax) xmax=hp_bound->x;
	       if(hp_bound->y > ymax) ymax=hp_bound->y;
	       if(hp_bound->x < xmin) xmin=hp_bound->x;
	       if(hp_bound->y < ymin) ymin=hp_bound->y;
	       if(hp_bound->tx > xmax) xmax=hp_bound->tx;
	       if(hp_bound->ty > ymax) ymax=hp_bound->ty;
	       if(hp_bound->tx < xmin) xmin=hp_bound->tx;
	       if(hp_bound->ty < ymin) ymin=hp_bound->ty;
	       hp_bound=hp_bound->next;
	}

	head.width=xmax+400-xmin;
	head.height=ymax+400-ymin;

	if(mark.flag){
	       mark.x=mark.x+200-xmin;
	       mark.y=mark.y+200-ymin;
	}

	hp_atom=dac_root.next;
	for(d=0; d<hp.nc; d++){
	       hp_atom->x=hp_atom->x-xmin+200;
	       hp_atom->y=hp_atom->y-ymin+200;
	       hp_atom=hp_atom->next;
	}

	hp_bound=da_root.next;
	for(d=0; d<hp.n; d++){
	       hp_bound->x=hp_bound->x-xmin+200;
	       hp_bound->y=hp_bound->y-ymin+200;
	       hp_bound->tx=hp_bound->tx-xmin+200;
	       hp_bound->ty=hp_bound->ty-ymin+200;
	       hp_bound=hp_bound->next;
	}
    }
}

void cut_end(int *x, int *y, int tx, int ty, int r)
{
    int dx, dy, v;
    int qx, qy;
    float alpha;

    dx=tx - *x;
    dy=ty - *y;
    v = calc_vector(dx, dy);
    alpha=(float) asin((float) dx/v);
    qx=r*sin(alpha);
    qy=r*cos(alpha);

    if( dx <0 && dy <0 ){
	*x=*x+qx;
	*y=*y-qy;
    }

    if( dx ==0 && dy <0 ){
	*y=*y-r;
    }

    if( dx >0 && dy <0 ){
	*x=*x+qx;
	*y=*y-qy;
    }

    if( dx <0 && dy ==0 ){
	*x=*x-r;
    }

    if( dx >0 && dy ==0 ){
	*x=*x+r;
    }

    if( dx <0 && dy >0 ){
	*x=*x+qx;
	*y=*y+qy;
    }

    if( dx ==0 && dy >0 ){
	*y=*y+r;
    }

    if( dx >0 && dy >0 ){
	*x=*x+qx;
	*y=*y+qy;
    }
}

struct xy_co *bound_cut(int x, int y, int tx, int ty, int r)
{
    struct xy_co *coord=&xycoord[0];
    struct dc *hp_a;
    int d, i, n; 
    char *hc;


    hp_a=dac_root.next;
    for(d=0; d<hp.nc; d++){
	hc=hp_a->c;
	n=strlen(hp_a->c);
	for(i=0;i<n;i++){
	    if(Extra_char(*hc)) {hc++;--n;}
	    hc++;
	}
	if(x == hp_a->x && y == hp_a->y)
	    if(hp_a->direct== -1) cut_end(&x, &y, tx, ty, r*n);
	    else cut_end(&x, &y, tx, ty, r);
	if(tx == hp_a->x && ty == hp_a->y)
	    if(hp_a->direct== -1) cut_end(&tx, &ty, x, y, r*n);
	    else cut_end(&tx, &ty, x, y, r);
	hp_a=hp_a->next; 
    }
    coord->x=x;
    coord->y=y;
    coord->tx=tx;
    coord->ty=ty;
    return(coord);
}

int move_pos(int x, int y, int tx, int ty)
{
    struct data *hp_bound;
    struct dc *hp_atom;
    int d, f=False;

    hp_bound=da_root.next;
    for(d=0; d<hp.n; d++){
	if(hp_bound->x == x && hp_bound->y == y){
		    hp_bound->x = tx; 
		    hp_bound->y = ty;
		    f=True;}
	if(hp_bound->tx == x && hp_bound->ty == y){
		    hp_bound->tx = tx; 
		    hp_bound->ty = ty;
		    f=True;}
	hp_bound=hp_bound->next;
	}

    hp_atom=dac_root.next;
    for(d=0; d<hp.nc; d++){
	if(hp_atom->x == x && hp_atom->y == y){
		    hp_atom->x = tx;
		    hp_atom->y = ty;
		    f=True;}
	hp_atom=hp_atom->next;
	}
	return f;
}

int partial_move(int x, int y, int tx, int ty, int dx, int dy)
{
    struct data *hp_bound;
    struct dc *hp_atom;
    int d, f=False;

    hp_bound=da_root.next;
    for(d=0; d<hp.n; d++){
        if(check_pos_rec(hp_bound->x, hp_bound->y, x, y, tx, ty)){
		    hp_bound->x = hp_bound->x + dx; 
		    hp_bound->y = hp_bound->y + dy; 
		    f=True;}
        if(check_pos_rec(hp_bound->tx, hp_bound->ty, x, y, tx, ty)){
		    hp_bound->tx = hp_bound->tx + dx; 
		    hp_bound->ty = hp_bound->ty + dy; 
		    f=True;}
	hp_bound=hp_bound->next;
	}
    
    hp_atom=dac_root.next;
    for(d=0; d<hp.nc; d++){
        if(check_pos_rec(hp_atom->x, hp_atom->y, x, y, tx, ty)){
		    hp_atom->x = hp_atom->x + dx; 
		    hp_atom->y = hp_atom->y + dy; 
		    f=True;}
	hp_atom=hp_atom->next;
	}
    return f;
}
