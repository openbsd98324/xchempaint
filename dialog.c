/*
 *		functions of the buttons and dialogs
 */


#include "chemtool.h"


void Puttext(Widget w, char *str)
{
    XawTextBlock text;
    static XawTextPosition pos = 0;

    text.length = strlen(str);
    text.ptr = str;
    text.firstPos = 0;
    text.format = FMT8BIT;

    XawTextReplace(w, pos, pos, &text);

    pos += text.length;
    XawTextSetInsertionPoint(w, pos);
}

Widget MakeStringBox(parent, name, string, lenght)
Widget parent;
String name, string;
int lenght;
{
    Arg args[5];
    Cardinal numargs = 0;
    Widget StringW;

    XtSetArg(args[numargs], XtNeditType, XawtextEdit); numargs++;
    XtSetArg(args[numargs], XtNstring, string); numargs++;
    XtSetArg(args[numargs], XtNwidth, lenght); numargs++;

    StringW = XtCreateManagedWidget(name, asciiTextWidgetClass,
    parent, args, numargs);
    return(StringW);
}

String GetString(w)
Widget w;
{
    String str;
    Arg arglist[1];

    XtSetArg(arglist[0], XtNstring, &str);
    XtGetValues( w, arglist, ONE);
    return(str);
}

void load_file()
{
    String filename = GetString(dialog);

    if(!load_mol(filename)) {
        FreePix();
	CreatePix();
	Display_Mol();
	}
    if(modify){
	XtPopdown(dialogbox);
	modify=False;
	}
}

void Load()
{
    Position x, y;
    Dimension w, h;

    if(modify){
        XtRemoveAllCallbacks(okdial, XtNcallback);
        XtAddCallback(okdial, XtNcallback, load_file, NULL);
	XtVaGetValues(toplevel, XtNwidth, &w, XtNheight, &h, NULL);
	XtTranslateCoords(toplevel, (Position) w/2, (Position) h/2, &x, &y);
	XtVaSetValues(dialogbox, XtNx, x, XtNy, y, NULL);
	XtPopup(dialogbox, XtGrabNonexclusive);
    }else load_file();
}

void Save()
{
    String filename = GetString(dialog);
	if(!save_mol(filename)) modify=False;
}

void Export()
{
    Position x, y;
    Dimension w, h;

    XtVaGetValues(toplevel, XtNwidth, &w, XtNheight, &h, NULL);
    XtTranslateCoords(toplevel, (Position) w/2, (Position) h/2, &x, &y);
    XtVaSetValues(exportdialog, XtNx, x, XtNy, y, NULL);
    XtPopup(exportdialog, XtGrabNonexclusive);
}

void Close_Export()
{
    XtPopdown(exportdialog);
}

void XBM_Export()
{
    String filename = GetString(dialog);
    export_bitmap(filename);
}

void Xfig_Export()
{
    String filename = GetString(dialog);
    export_xfig(filename);
}

void LaTeX_Export()
{
    String filename = GetString(dialog);
    export_latex_pic(filename);
}

void Really_Quit(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
    XtPopdown(dialogbox);
    if((int) client_data) exit(0);
}

void Quit()
{
    Position x, y;
    Dimension w, h;

    if(modify){
        XtRemoveAllCallbacks(okdial, XtNcallback);
        XtAddCallback(okdial, XtNcallback, Really_Quit, (XtPointer)1);
	XtVaGetValues(toplevel, XtNwidth, &w, XtNheight, &h, NULL);
	XtTranslateCoords(toplevel, (Position) w/2, (Position) h/2, &x, &y);
	XtVaSetValues(dialogbox, XtNx, x, XtNy, y, NULL);
	XtPopup(dialogbox, XtGrabNonexclusive);
    }else exit(0);
}

void Zoom(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
    if((int) client_data) {
      if(zoom_factor) {
	zoom_factor=--zoom_factor;
	FreePix();
	CreatePix();
	Display_Mol();
	}
    }else{
      if(zoom_factor < 2) {
	zoom_factor=++zoom_factor;
	FreePix();
	CreatePix();
	Display_Mol();
	}
    }
}

void Change_Text(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
    text_direct= (int) client_data;
    XtUninstallTranslations(pix);
    XtOverrideTranslations(pix ,catom);
}

void Change_Angle(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
    draw_angle= (int) client_data;
    XtUninstallTranslations(pix);
    XtOverrideTranslations(pix ,ctrans1);
}

void Edit_Mol(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
    XtUninstallTranslations(pix);
    XtOverrideTranslations(pix, crec);
    switch((int) client_data)
    {
     case 0:
      XtOverrideTranslations(pix, cmark);
      break;
     case 1:
      XtOverrideTranslations(pix, cdel);
      break;
     case 2:
      XtUninstallTranslations(pix);
      XtOverrideTranslations(pix, cmbound);
      break;
     case 3:
      XtUninstallTranslations(pix);
      XtOverrideTranslations(pix, cpmove);
      break;
    }
}

void UnMark(void)
{
    mark.flag=False;
    Display_Mol();
}

Widget toggle_icon(char *name,Widget tb, Widget pw, char *bitmap,
		    int width, int height)
{
    Pixel fg, bg;
    Display *display=XtDisplay(pw);

    Widget w=XtVaCreateManagedWidget(name, toggleWidgetClass,
                pw, XtNradioGroup, tb, NULL);
    XtVaGetValues(w, XtNforeground, &fg, XtNbackground, &bg, NULL);
    XtVaSetValues(w, XtNbitmap,
		XCreatePixmapFromBitmapData(display, 
		XDefaultRootWindow(display), bitmap, width, height, fg, bg,
		XDefaultDepthOfScreen(XtScreen(w))), NULL);
    return w;
}
