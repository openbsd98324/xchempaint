/*
 *				Chemtool version 1.0
 *
 *	written by Thomas Volk 
 *	Jan 1998
 *
 *	You can use Chemtool under the terms 
 *	of the GNU General Public License
 *
 *	This software comes with ABSOLUTELY NO WARRANTY
 */

 
#include "chemtool.h"
#include "bitmap.h"


Arg CanvasArg[] = {
    {XtNwidth,(XtArgVal)500},{XtNheight,(XtArgVal)350},
    {XtNborderWidth,(XtArgVal)1},
    {XtNbottom,(XtArgVal)XawChainBottom},{XtNtop,(XtArgVal)XawChainTop},
    {XtNleft,(XtArgVal)XawChainLeft},{XtNright,(XtArgVal)XawChainRight},
    {XtNallowHoriz,(XtArgVal)True},{XtNallowVert,(XtArgVal)True}};

Arg RadioSelectArg[] = {XtNstate,(XtArgVal)True};

XGCValues values;
XtGCMask mask = GCForeground | GCBackground | GCLineStyle | GCFunction;
Pixmap 	picture;
GC 	gc, cleargc, invgc, drawgc;
XFontStruct *font;
Widget  toplevel, buttonbox, pixbox, rootpane, pbox,
	drawpanel, filepanel, viewpanel, textpanel,
	left, middle, right,
	pent1, pent2, hexa, octa,
	load, save, export, quit, figexport, latexexport, xbmexport,
	exportdialog, expb, closeexport, explabel,
	zoomin, zoomout,
	markmol, delete, center, unmarkmol, mbound, pmove,
	canvas, pix, 
	dialog, atom_dialog,
	dialogbox, dialb, diall, okdial, canceldial;

XtTranslations ctrans1, catom, crec, cdel, cmark, cmbound, cpmove;
String 	string;

Pixel fgr=0, bgr=1;		/* fore- and background-colors of the pixmap */
int   draw_angle=Hex;		/* angletype of the draw function */
int   text_direct=Middle_Text;	/* alignment of the textinsert function */
int   font_type=3;		/* fontsize */
int   line_type=0;		/* linestyle */
int   modify=False;		/* modification-flag */
float size_factor=0.6;		/* cellsize of the pixmapgraphig */
int   zoom_factor=0;		/* flag for the zoom-in/out function */

/*
 * 	Main function
 */

main(argc, argv)

int argc;
char **argv;
{
    XtAppContext app_context;

    String dialog_trans = "<Key>Return:		end-of-line() \n\
			   <Key>Tab:		end-of-line() \n\
			   <Key>space:		end-of-line()";
    XtTranslations cdialog;
    String radio_trans = "#override \n\
    <LeaveWindow>:		unhighlight() \n\
    <EnterWindow>:		highlight(Always) \n\
    <Btn1Down>,<Btn1Up>:	set() notify()";
    XtTranslations cradio;

    String tr_bounds_1 = "<Expose>:		Display_Mol() \n\
		    <Btn1Down>:			Set_vector() \n\
		    <Btn1Up>:			Add_vector() \n\
		    <Btn1Motion>:		Put_vector() \n\
		    <Btn2Down>:			Add_double() \n\
		    <Btn3Down>:			Del_vector() \n\
		    <Key>t: 			Add_atom() \n\
		   ";
    String tr_atom = "<Expose>:			Display_Mol() \n\
		    <Btn1Down>:			Add_atom() \n\
		    <Btn2Down>:			Add_atom() \n\
		    <Btn3Down>:			Del_atom() \n\
		   ";
    String tr_rec = "<Expose>:			Display_Mol() \n\
		    <Btn1Motion>:		Put_rec() \n\
		    <Btn1Down>:			Set_start_rec() \n\
		    <Btn1Up>:			Mark_rec() \n\
		   ";
    String tr_del ="<Btn1Up>:			Del_rec()";
    String tr_mark ="<Btn1Up>:			Mark_rec()";
    String tr_mbound ="<Expose>:		Display_Mol() \n\
		       <Btn1Down>:		Add_double()";
    String tr_pmove ="<Expose>:                Display_Mol() \n\
		    <Btn1Motion>:		Put_pmove() \n\
		    <Btn1Down>:			Set_pmove()";

    XtActionsRec win_action[] = {
	{ "Display_Mol",Display_Mol},
	{ "Set_vector",	Set_vector},
	{ "Add_vector",	Add_vector},
	{ "Put_vector",	Put_vector},
	{ "Del_vector",	Del_vector},
	{ "Add_double",	Add_double},
	{ "Add_atom",	Add_atom},
	{ "Del_atom",	Del_atom},
	{ "Set_start_rec",	Set_start_rec},
	{ "Del_rec",	Del_rec},
	{ "Mark_rec",	Mark_rec},
	{ "Put_rec",	Put_rec},
	{ "Set_pmove",	Set_pmove},
	{ "Put_pmove",	Put_pmove} 
	};

    setup_data();

    toplevel = XtVaAppInitialize(
	&app_context,
	"Chemtool",
	NULL,0,
	&argc,argv,
	NULL,
	NULL);


    exportdialog = XtVaCreatePopupShell("Chemtool", transientShellWidgetClass,
    toplevel, NULL, 0);

    expb = XtCreateManagedWidget( "expb", boxWidgetClass,
	    exportdialog, NULL, 0); 

    explabel = XtCreateManagedWidget( "Export to", labelWidgetClass, 
	    expb, NULL, 0); 

    xbmexport = XtCreateManagedWidget("XBM Bitmap", commandWidgetClass,
	    expb, NULL, 0);
    XtAddCallback(xbmexport, XtNcallback, XBM_Export, NULL);

    figexport = XtCreateManagedWidget("   xfig   ", commandWidgetClass,
	    expb, NULL, 0);
    XtAddCallback(figexport, XtNcallback, Xfig_Export, NULL);

    latexexport = XtCreateManagedWidget("  PicTeX  ", commandWidgetClass,
	    expb, NULL, 0);
    XtAddCallback(latexexport, XtNcallback, LaTeX_Export, NULL);

    closeexport = XtCreateManagedWidget("  close   ", commandWidgetClass,
		expb, NULL, 0);
    XtAddCallback(closeexport, XtNcallback, Close_Export, NULL);


    dialogbox = XtVaCreatePopupShell("Chemtool", transientShellWidgetClass,
    toplevel, NULL, 0);

    dialb = XtCreateManagedWidget( "dialb", boxWidgetClass,
	    dialogbox, NULL, 0); 

    diall = XtCreateManagedWidget( "discard changes ?", labelWidgetClass, 
	    dialb, NULL, 0); 

    okdial = XtCreateManagedWidget("ok", commandWidgetClass,
		dialb, NULL, 0);

    canceldial = XtCreateManagedWidget("cancel", commandWidgetClass,
		dialb, NULL, 0);
    XtAddCallback(canceldial, XtNcallback, Really_Quit, (XtPointer)0);
    XtAddCallback(okdial, XtNcallback, Really_Quit, (XtPointer)1);


    rootpane = XtVaCreateManagedWidget("rootpane", panedWidgetClass, toplevel,
      XtNorientation, XtorientVertical, NULL, 0); 
    
    buttonbox = XtVaCreateManagedWidget("buttonbox", panedWidgetClass, rootpane,
      /* XtNheight, 150, XtNwidth, 600, */
      XtNorientation, XtorientHorizontal,
      XtNshowGrip, False, NULL, 0); 
    
    filepanel = XtVaCreateManagedWidget("filepanel", boxWidgetClass, buttonbox,
		XtNshowGrip, False, NULL, 0);

    load = XtCreateManagedWidget("Load", commandWidgetClass,
	    filepanel, NULL, 0);
    XtAddCallback(load, XtNcallback, Load, NULL);

    save = XtCreateManagedWidget("Save", commandWidgetClass,
	    filepanel, NULL, 0);
    XtAddCallback(save, XtNcallback, Save, NULL);

    export = XtCreateManagedWidget("Export", commandWidgetClass,
                filepanel, NULL, 0);
    XtAddCallback(export, XtNcallback, Export, NULL);

    quit = XtCreateManagedWidget("Quit", commandWidgetClass,
	    filepanel, NULL, 0);
    XtAddCallback(quit, XtNcallback, Quit, NULL);

    dialog = MakeStringBox(filepanel, "dialog", string, 210);
    cdialog = XtParseTranslationTable(dialog_trans);
    XtOverrideTranslations(dialog ,cdialog);

/*
    viewpanel = XtVaCreateManagedWidget("viewpanel", boxWidgetClass, buttonbox,
		XtNshowGrip, False, NULL, 0);
*/
    zoomin = XtCreateManagedWidget("zoom in ", commandWidgetClass,
		filepanel, NULL, 0);
    XtAddCallback(zoomin, XtNcallback, Zoom, (XtPointer)0);

    zoomout = XtCreateManagedWidget("zoom out", commandWidgetClass,
		filepanel, NULL, 0);
    XtAddCallback(zoomout, XtNcallback, Zoom, (XtPointer)1);

    center = XtCreateManagedWidget("center", commandWidgetClass,
		filepanel, NULL, 0);
    XtAddCallback(center, XtNcallback, Center, (XtPointer)0);



    drawpanel = XtVaCreateManagedWidget("drawpanel", boxWidgetClass, buttonbox,
		XtNshowGrip, False, NULL, 0);

    hexa = toggle_icon("hexa", NULL, drawpanel, 
			hex_bits, hex_width, hex_height);
    XtAddCallback(hexa, XtNcallback, Change_Angle, (XtPointer)Hex);

    pent1 = toggle_icon("pent1", hexa, drawpanel, 
			pent1_bits, pent1_width, pent1_height);
    XtAddCallback(pent1, XtNcallback, Change_Angle, (XtPointer)Pent);

    pent2 = toggle_icon("pent2", pent1, drawpanel, 
			pent2_bits, pent2_width, pent2_height);
    XtAddCallback(pent2, XtNcallback, Change_Angle, (XtPointer)Pent_switch);

    octa = toggle_icon("octa", pent2, drawpanel, 
			octa_bits, octa_width, octa_height);
    XtAddCallback(octa, XtNcallback, Change_Angle, (XtPointer)Octa);

    left = toggle_icon("left", octa, drawpanel, 
			text_left_bits, text_left_width, text_left_height);
    XtAddCallback(left, XtNcallback, Change_Text, (XtPointer)Left_Text);

    middle = toggle_icon("middle", left, drawpanel, 
		text_middle_bits, text_middle_width, text_middle_height);
    XtAddCallback(middle, XtNcallback, Change_Text, (XtPointer)Middle_Text);

    right = toggle_icon("right", middle, drawpanel, 
		text_right_bits, text_right_width, text_right_height);
    XtAddCallback(right, XtNcallback, Change_Text, (XtPointer)Right_Text);

    pmove = toggle_icon("pmove", right, drawpanel, 
		pmove_bits, pmove_width, pmove_height);
    XtAddCallback(pmove, XtNcallback, Edit_Mol, (XtPointer)3);

    atom_dialog = MakeStringBox(drawpanel, "atom_dialog", string, 314);
    XtOverrideTranslations(atom_dialog ,cdialog);

    unmarkmol = XtCreateManagedWidget(" unmark ", commandWidgetClass,
		drawpanel, NULL, 0);
    XtAddCallback(unmarkmol, XtNcallback, UnMark, (XtPointer)0);

    markmol = XtVaCreateManagedWidget(" mark ", toggleWidgetClass,
	    drawpanel, XtNradioGroup, pmove, NULL);
    XtAddCallback(markmol, XtNcallback, Edit_Mol, (XtPointer)0);

    delete = XtVaCreateManagedWidget("  delete  ", toggleWidgetClass,
	    drawpanel, XtNradioGroup, markmol, NULL);
    XtAddCallback(delete, XtNcallback, Edit_Mol, (XtPointer)1);

    mbound = XtVaCreateManagedWidget("Bound-Type", toggleWidgetClass,
	    drawpanel, XtNradioGroup, delete, NULL);
    XtAddCallback(mbound, XtNcallback, Edit_Mol, (XtPointer)2);

    textpanel = XtVaCreateManagedWidget("textpanel", boxWidgetClass, 
    buttonbox, XtNshowGrip, False, NULL, 0);

    cradio=XtParseTranslationTable(radio_trans);
    XtOverrideTranslations(right, cradio);
    XtOverrideTranslations(middle, cradio);
    XtOverrideTranslations(left, cradio);
    XtOverrideTranslations(hexa, cradio);
    XtOverrideTranslations(pent1, cradio);
    XtOverrideTranslations(pent2, cradio);
    XtOverrideTranslations(markmol, cradio);
    XtOverrideTranslations(delete, cradio);
    XtOverrideTranslations(mbound, cradio);
    XtOverrideTranslations(octa, cradio);
    XtOverrideTranslations(pmove, cradio);

    catom=XtParseTranslationTable(tr_atom);
    ctrans1=XtParseTranslationTable(tr_bounds_1);
    crec=XtParseTranslationTable(tr_rec);
    cdel=XtParseTranslationTable(tr_del);
    cmark=XtParseTranslationTable(tr_mark);
    cmbound=XtParseTranslationTable(tr_mbound);
    cpmove=XtParseTranslationTable(tr_pmove);
	    
    mark.flag=False;

    pixbox = XtCreateManagedWidget( "chemtool display", formWidgetClass,
	    rootpane, NULL, 0);
    
    canvas = XtVaCreateManagedWidget( "canvas", viewportWidgetClass,
	    pixbox, NULL, 0);
    XtSetValues(canvas, CanvasArg, XtNumber(CanvasArg));

    head.width=PIXMAPWIDTH;	/* set the default pixmap width and height */
    head.height=PIXMAPHEIGHT;	/* by starting chemtool whitout filename   */	

    pbox = XtCreateManagedWidget( "pbox", boxWidgetClass,
                canvas, NULL, 0);

    pix = XtVaCreateManagedWidget("bitmap",
        simpleWidgetClass,
        pbox, XtNtranslations, ctrans1,
        XtNwidth, PIXMAPWIDTH, XtNheight, PIXMAPHEIGHT,
        NULL);

    fgr=BlackPixel(XtDisplay(pix),DefaultScreen(XtDisplay(pix)));
    bgr=WhitePixel(XtDisplay(pix),DefaultScreen(XtDisplay(pix)));
      
    values.foreground=fgr;
    values.background=bgr;
    values.line_style=LineSolid;
    values.function=GXcopy;
    gc = XtGetGC(pix, mask, &values);

    if(argc >1) {		/* load file */
	Puttext(dialog, *++argv);
	load_mol(*argv);
	}

    CreatePix();		/* build pixmap */

    XtSetValues(hexa,RadioSelectArg,XtNumber(RadioSelectArg));
    XtOverrideTranslations(pix, ctrans1);

    XtAppAddActions(app_context, win_action, XtNumber(win_action));
    XtRealizeWidget(toplevel);

    XtAppMainLoop(app_context);
}
