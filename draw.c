/*
 *	draw functions inside the pixmap
 */


#include "chemtool.h"


void Add_atom(w, event, params, num_params)
Widget w;
XButtonEvent *event;
String *params;
Cardinal *num_params;
{
    struct xy_co *coord;
    char *name = GetString(atom_dialog);
    int x, y;

    if(strlen(name)) {
	x=myround(event->x, size_factor);
	y=myround(event->y, size_factor);
	coord=position(x, y, x, y, No_angle);

	add_char(coord->x, coord->y, name, text_direct);
	modify=True;
	Display_Mol();
    }
}

void Del_atom(w, event, params, num_params)
Widget w;
XButtonEvent *event;
String *params;
Cardinal *num_params;
{
    del_char(select_char(event->x, event->y, size_factor));
    modify=True;
    Display_Mol();
}

void Set_vector(w, event, params, num_params)
Widget w;
XButtonEvent *event;
String *params;
Cardinal *num_params;
{
    int angle_count[]={0, 12, 10, 10, 8};
    int d, dmax;
    struct xy_co *coord;

    hp.x=myround(event->x, size_factor);
    hp.y=myround(event->y, size_factor);
    coord=position(hp.x, hp.y, hp.x, hp.y, draw_angle);
    hp.x=coord->x;
    hp.y=coord->y;
    hp.tx=hp.x;
    hp.ty=hp.y;
    dmax=angle_count[draw_angle];
    for(d=0;d<dmax;d++){
	coord=calc_angle(hp.x, hp.y, draw_angle, d);
	XDrawRectangle(XtDisplay(w), picture, invgc,
	coord->x*size_factor-2, coord->y*size_factor-2, 4, 4);
	}
    CopyPlane();
}

void Add_vector(w, event, params, num_params)
Widget w;
XButtonEvent *event;
String *params;
Cardinal *num_params;
{
    if((hp.x-hp.tx) || (hp.y-hp.ty))
      {
      add_struct(hp.x, hp.y, hp.tx, hp.ty, 0);
      }
    Display_Mol();
    modify=True;
}

void Put_vector(w, event, params, num_params)
Widget w;
XButtonEvent *event;
String *params;
Cardinal *num_params;
{
    int x,y;
    struct xy_co *coord;

    x=myround(event->x, size_factor);
    y=myround(event->y, size_factor);
    coord=position(x, y, hp.x, hp.y, draw_angle);
    x=coord->x;
    y=coord->y;
    if(x != hp.x || y != hp.y){
	XDrawLine(XtDisplay(w), picture, invgc, hp.x*size_factor, 
	hp.y*size_factor, hp.tx*size_factor, hp.ty*size_factor);
	hp.tx=x;
	hp.ty=y;
	XDrawLine(XtDisplay(w), picture, invgc, hp.x*size_factor, 
	hp.y*size_factor, hp.tx*size_factor, hp.ty*size_factor);
    	CopyPlane();
    }
}

void Del_vector(w, event, params, num_params)
Widget w;
XButtonEvent *event;
String *params;
Cardinal *num_params;
{
    del_struct(select_vector(event->x, event->y, size_factor));
    modify=True;
    Display_Mol();
}

void Add_double(w, event, params, num_params)
Widget w;
XButtonEvent *event;
String *params;
Cardinal *num_params;
{
    struct data *hdc;
    int dx, dy;

    hdc=select_vector(event->x, event->y, size_factor);
    if(hdc) {
    	dx=hdc->x - hdc->tx;
    	dy=hdc->y - hdc->ty;
	if(!dx || !dy){
	    if(hdc->bound>=4) hdc->bound=0;
	    else ++hdc->bound;
	} else {
	    if(hdc->bound>=3) hdc->bound=0;
	    else ++hdc->bound;
	}
    modify=True;
    }
    Display_Mol();
}

void Set_start_rec(w, event, params, num_params)
Widget w;
XButtonEvent *event;
String *params;
Cardinal *num_params;
{
    hp.x=event->x;
    hp.y=event->y;
    hp.tx=event->x;
    hp.ty=event->y;
}

void Put_rec(w, event, params, num_params)
Widget w;
XButtonEvent *event;
String *params;
Cardinal *num_params;
{
    XDrawRectangle(XtDisplay(w), picture, invgc, hp.x, hp.y, 
	hp.tx-hp.x, hp.ty-hp.y); 
    hp.tx=event->x;
    hp.ty=event->y;
    if(hp.tx<hp.x) hp.tx=hp.x;
    if(hp.ty<hp.y) hp.ty=hp.y;
    XDrawRectangle(XtDisplay(w), picture, invgc, hp.x, hp.y, 
	hp.tx-hp.x, hp.ty-hp.y); 
    CopyPlane();
}

void Del_rec(w, event, params, num_params)
Widget w;
XButtonEvent *event;
String *params;
Cardinal *num_params;
{
    if(hp.tx<hp.x) hp.tx=hp.x;
    if(hp.ty<hp.y) hp.ty=hp.y;
    XDrawRectangle(XtDisplay(w), picture, invgc, hp.x, hp.y, 
	    hp.tx-hp.x, hp.ty-hp.y); 
    CopyPlane();
    if(hp.tx==hp.x && hp.ty==hp.y){
	del_char(select_char(hp.x, hp.y, size_factor));
	del_struct(select_vector(hp.x, hp.y, size_factor));
	Display_Mol(); modify=True;
    }else{
	partial_delete(myround(hp.x, size_factor), myround(hp.y, size_factor), 
	      myround(hp.tx, size_factor), myround(hp.ty, size_factor));
	Display_Mol(); modify=True;
    }
}

void Mark_rec(w, event, params, num_params)
Widget w;
XButtonEvent *event;
String *params;
Cardinal *num_params;
{
    if(hp.tx<hp.x) hp.tx=hp.x;
    if(hp.ty<hp.y) hp.ty=hp.y;
    XDrawRectangle(XtDisplay(w), picture, invgc, hp.x, hp.y, 
	    hp.tx-hp.x, hp.ty-hp.y); 
    CopyPlane();
    if(hp.tx==hp.x && hp.ty==hp.y){
	mark.flag=False;
    }else{
	mark.x=myround(hp.x, size_factor);
	mark.y=myround(hp.y, size_factor);
	mark.w=myround(hp.tx-hp.x, size_factor);
	mark.h=myround(hp.ty-hp.y, size_factor);
	mark.flag=True;
    }
    Display_Mol();
}

void Center()
{
    FreePix();
    center_mol();
    CreatePix();
    Display_Mol();
    modify=True;
}

void Set_pmove(w, event, params, num_params)
Widget w;
XButtonEvent *event;
String *params;
Cardinal *num_params;
{
    struct xy_co *coord;
    int x, y;

    x=myround(event->x, size_factor);
    y=myround(event->y, size_factor);
    coord=position(x, y, x, y, No_angle);
    hp.x=coord->x;
    hp.y=coord->y;
}

void Put_pmove(w, event, params, num_params)
Widget w;
XButtonEvent *event;
String *params;
Cardinal *num_params;
{
    int x, y;
    int dx, dy;

    x=myround(event->x, size_factor);
    y=myround(event->y, size_factor);

    if(mark.flag){
	if(check_pos_rec(x, y, mark.x, mark.y, mark.w+mark.x, mark.y+mark.h)){
	    dx=x-hp.x;
	    dy=y-hp.y;
	    if(partial_move(mark.x, mark.y, mark.w+mark.x, 
			    mark.y+mark.h, dx, dy))modify=True;
	    mark.x=mark.x+dx;
	    mark.y=mark.y+dy;
	    hp.x=x;
	    hp.y=y;
	    Display_Mol();
	}
    }else{
	if(move_pos(hp.x, hp.y, x, y)){
	    hp.x=x;
	    hp.y=y;
	    Display_Mol();
	    modify=True;
	    }
    }
}
